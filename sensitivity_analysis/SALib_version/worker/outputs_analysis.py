#!/usr/bin/env python
import struct
import time
import os
import json
import sys
from sys import argv
import psutil
import numpy as np
import pickle as pkl
from SALib.analyze import sobol

# Définition du pas de temps à prendre
#time_step = int(argv[1])
if not(len(argv) >= 8):
    print('Usage: outputs_analysis.py <problem.pkl> <input> <name template> <output> <start> <end>')
    sys.exit()
problem = pkl.load(open(argv[1] ,'rb'))
input_dir_path = argv[2]
name_template = argv[3]
input_file_path_template = os.path.join(input_dir_path, name_template)
output_path = argv[4]
step_beg = int(argv[5])
step_end = int(argv[6])
num_runs = int(argv[7])

'''
Initialisation
'''
i = 0
file = input_file_path_template.format(i)
assert(os.path.exists(file))
meta_file = open(file + '_meta')
names_vars = meta_file.read().split('\n')
num_columns = len(names_vars)
num_steps = step_end - step_beg
num_steps_total = 401
run_size = 8 * num_steps_total * num_columns
print("Number of columns:", num_columns)

runs = []
process = psutil.Process(os.getpid())

all_runs = np.ndarray((num_runs, num_steps, num_columns))

run_no_total = 0

while True:
    file = input_file_path_template.format(i)
    i += 1
    if not os.path.exists(file):
        break

    num_runs = os.stat(file).st_size / run_size
    if not num_runs.is_integer() or num_runs == 0:
        raise Exception('Incorrect file size for ' + file)
    else:
        num_runs = int(num_runs)
    print('Number of runs in', file, ':', num_runs)

    start_time = time.time()
    run_file = open(file, 'rb')

    '''
    Reading all runs values from all files for all time steps
    '''
    for run_no in range(num_runs):
        for time_step in range(num_steps):
            real_time_step = step_beg + time_step
            run_offset = run_size * run_no
            inner_offset = num_columns * real_time_step * 8
            offset = run_offset + inner_offset
            values = []
            run_file.seek(offset)
            for v in range(num_columns):
                value = struct.unpack('d', run_file.read(8))[0]
                all_runs[run_no_total, time_step, v] = value
        run_no_total += 1

    run_file.close()
    print('Time:', time.time() - start_time)
    print('RAM usage', process.memory_info().rss)
    sys.stdout.flush()

print('Shape of all runs:', all_runs.shape)
print('Number of runs loaded:', run_no_total)

# vector_no (output) ; time_step ; indices and incertitudes ; num vars (input)
Si = np.ndarray((num_columns, num_steps, 4, problem['num_vars']))

print('Start analysis', time.time())
# iterate over time steps
for time_step in range(num_steps):
    print('Time step', time_step)
    sys.stdout.flush()
    
    for v in range(num_columns):
        start_time = time.time()
        try:
            Si_b = np.stack(sobol.analyze(problem, all_runs[:, time_step, v], calc_second_order = False).values())
            Si[v, time_step, ...] = Si_b
        except ZeroDivisionError:
            print('ZeroDivisionError v=', v)
            sys.stdout.flush()
        
        print('Duration:', time.time() - start_time)

Si.dump(output_path)

