#!/usr/bin/env python
import sys

if len(sys.argv) < 2:
    print('usage: run.py input_file_path output_file_path')
    exit(1)
else:
    input_file_path = sys.argv[1]
    output_file_path = sys.argv[2]

import time
import signal
import struct
import json
import pysd
import numpy as np

def display(*args, **kwargs):
    sep = kwargs['sep'] if 'sep' in kwargs else ' '
    print('[PY ' + time.ctime() + '] ' + sep.join(map(str, args)), **kwargs)
    sys.stdout.flush()

display('Runs init')
display('Output file path:', output_file_path)
display('Loading input set', input_file_path)

with open('problem.json', 'r') as f:
    problem = json.load(f)

w3 = pysd.load(problem['world3_path'])
components = problem['components']

params_values = np.load(input_file_path)
num_runs = len(params_values)

# Sauvegarde à chaque signal (on ne peut pas handle le SIGKILL)
def signal_handler(signal, frame):
    display('Handling signal', signal)
    display_stats()
    exit(0)

signals = ['SIGINT', 'SIGUSR1', 'SIGUSR2', 'SIGTERM', 'SIGXCPU', 'SIGXFSZ']
for s in signals:
    try:
        signal.signal(getattr(signal, s), signal_handler)
        display('Signal', s, 'OK')
    except:
        display('Signal', s, 'NO OK')

def display_stats():
    global i, params_values, start_time
    current_time = time.time()
    display(f'''
==============
Stats:
total runs   : {i + 1} / {num_runs}
time elapsed : {current_time - start_time}
avg per run  : {(current_time - start_time) / i}
==============
''')

# "Flush" dans le cas d'un fichier : on append à chaque itération en local
def append_stocks_to_file(stocks, run_file):
    for index, row in stocks.iterrows():
        for v in row:
            run_file.write(struct.pack('d', v))

run_file = open(output_file_path, 'wb')

display('Runs start! Total:', num_runs, 'runs')

start_time = time.time()
for i in range(num_runs):
    stocks = w3.run(params_values[i], return_columns = components)
    append_stocks_to_file(stocks, run_file)
run_file.close()

display('Runs end!')
display_stats()
