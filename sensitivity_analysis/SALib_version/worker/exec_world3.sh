#!/usr/local/bin/bash -l

#$ -P P_liris       # SPS liris
#$ -j y             # redirect stderr in stdout
#$ -l os='cl7'      # Linux
#$ -N World3_runs   # Job name
#$ -V               # export env
#$ -l sps=1
#$ -t 1-1:1

#$ -q long

echo "Task id = $SGE_TASK_ID"

echo "Début du job : `date`"
W3_SA_PATH="/home/steep/world3-03_python/sensitivity_analysis/SALib_version/worker/SA5000"
WORKER_ID=$((SGE_TASK_ID - 1))
INPUT_FILENAME="param_values_${{WORKER_ID}}.pkl"
INPUT="$W3_SA_PATH/param_values/$INPUT_FILENAME"
SCRIPT_PATH="$W3_SA_PATH/run.py"

# Là où est stocké le fichier de sortie sur le scratch
RUN_TMP_FILE=$TMPDIR/runs_tmp

# Là où sera copié le fichier de sortie dans SPS
OUTPUT_FILE="$W3_SA_PATH/res/stocks_$WORKER_ID"

# Run the simulation
$SCRIPT_PATH $INPUT $RUN_TMP_FILE

echo "Copies"
cp -v $RUN_TMP_FILE $OUTPUT_FILE

echo "Fin du job : `date`"
