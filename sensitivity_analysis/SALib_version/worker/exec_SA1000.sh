#!/usr/local/bin/bash -l

#$ -P P_liris                # SPS liris
#$ -j y                      # redirect stderr in stdout
#$ -l os='cl7'               # Linux
#$ -N World3_analysis        # Job name
#$ -V                        # export env
#$ -l sps=1
# $ -t 1-402:1

#$ -q mc_highmem_long

# $TMPDIR # local scratch

echo "Task id = $SGE_TASK_ID"

echo "Début du job : `date`"
W3_SA_PATH="/sps/inter/liris/rasoldie/w3_sensitivity_analysis"
STEP_BEG=$1
STEP_END=$2
SCRIPT_PATH="$W3_SA_PATH/outputs_analysis.py"
N=$3

PROBLEM="$W3_SA_PATH/problem.pkl"
#INPUT="$W3_SA_PATH/res"
INPUT="$W3_SA_PATH/res1000"
OUTPUT="$W3_SA_PATH/sobol_indices1000/${STEP_BEG}.pkl"
NAME_TEMPLATE="stocks_w{}"
SHM_PATH=/dev/shm

# Initialization

echo "Suppression"
rm -f $SHM_PATH/*

echo "Copie : "

cp -v $INPUT/* $SHM_PATH

# Run the simulation
$SCRIPT_PATH $PROBLEM $SHM_PATH $NAME_TEMPLATE $OUTPUT $STEP_BEG $STEP_END $N

echo "Fin du job : `date`"
