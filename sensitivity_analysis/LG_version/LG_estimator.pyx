# -*- coding: utf-8 -*-
"""
Created on Tue May 2017

@author: Laurent
"""
from __future__ import division

import numpy as np
cimport numpy as c_np

import cython
from libc.math cimport sqrt

@cython.boundscheck(False) # turn of bounds-checking for entire function
@cython.cdivision(True) # to avoid the exception checking
@cython.wraparound(False)

def LG_estimator(double[:, ::1] Y, double[::1] MeanY2, long[::1] ind1, long[::1] ind2, long p):
    
    cdef:
        int i, k, nrow, ncol, i_1, i_2
        double a, b, c, b_inter
    
    S = np.empty(p)
    cdef double[::1] vS = S
    
    nrow = Y.shape[0]
    ncol = p
    
    for k in xrange(ncol):
        
        a = 0.0
        b = 0.0
        c = 0.0
        b_inter = 0.0
        
        for i in xrange(nrow):
            
            i_1 = ind1[k]
            i_2 = ind2[k]
            
            if Y[i,i_1] != -1000.0 and Y[i,i_2] != -1000.0:
                
                a += (Y[i,i_1]-MeanY2[k])*(Y[i,i_2]-MeanY2[k])
                b_inter = (Y[i,i_1]+Y[i,i_2])/sqrt(2.0)-(sqrt(2.0)+1)*MeanY2[k]
                b += b_inter * b_inter
                c += Y[i,i_1]*Y[i,i_2]
            

        vS[k] = a/(b-c)

    return S


#def LG_estimator(c_np.ndarray[DTYPE_t, ndim=2] Y, c_np.ndarray[DTYPE_t, ndim=1] MeanY2, c_np.ndarray[DTYPE2_t, ndim=1] ind1, c_np.ndarray[DTYPE2_t, ndim=1] ind2, int p):
#	
#    assert Y.dtype == DTYPE and MeanY2.dtype == DTYPE and ind1.dtype == DTYPE2 and ind2.dtype == DTYPE2
#    
#    cdef int i, k, nrow, ncol
#    
#    nrow = Y.shape[0]
#    ncol = p
#    
#    cdef DTYPE_t a, b, c, b_inter
#    
#    cdef DTYPE2_t i_1, i_2
#    
#    cdef c_np.ndarray[DTYPE_t,ndim=1] S = np.zeros(p, dtype=DTYPE)
#    
#    for k in xrange(ncol):
#        
#        a = 0.0
#        b = 0.0
#        c = 0.0
#        b_inter = 0.0
#        
#        for i in xrange(nrow):
#            
#            i_1 = ind1[k]
#            i_2 = ind2[k]
#            
#            if Y[i,i_1] != -1000.0 and Y[i,i_2] != -1000.0:
#                
#                a += (Y[i,i_1]-MeanY2[k])*(Y[i,i_2]-MeanY2[k])
#                b_inter = (Y[i,i_1]+Y[i,i_2])/sqrt(2.0)-(sqrt(2.0)+1)*MeanY2[k]
#                b += b_inter * b_inter
#                c += Y[i,i_1]*Y[i,i_2]
#            
#
#        S[k] = a/(b-c)
#
#    return S
