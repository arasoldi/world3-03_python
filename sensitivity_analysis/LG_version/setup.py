# -*- coding: utf-8 -*-
"""
Created on April 5 2017

@author: Laurent
"""

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy

ext_modules = [Extension("LG_estimator", ["LG_estimator.pyx"],
               include_dirs=[numpy.get_include()])]

setup(cmdclass = {'build_ext': build_ext}, ext_modules = ext_modules)

# call: python setup.py build_ext --inplace