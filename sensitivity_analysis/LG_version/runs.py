import numpy as np, sys, signal, pysd, time, pickle, warnings
from numpy.random import *
from Saltelli import Saltelli
from uuid import uuid4

'''
Configuration
'''
# Chemins
w3_path = 'world3.py'
outputs_path = '' # répertoire de destination des fichiers de sortie avec le '/' à la fin si non vide
time_between_saving = 3600 # temps entre sauvegardes automatiques en secondes
multiproc = True

# Nombre d'échantillons N
n = 10000 # conseillé : 10000

# ID de la machine (pour nom des fichiers)
server_id = uuid4()

# Liste de tous les paramètres constants du modèle (pour les supprimer de la sortie)
const_params = ['GDP pc unit', 'unit agricultural input', 'unit population', 'ha per Gha', 'ha per unit of pollution', 'one year', 'Ref Hi GDP', 'Ref Lo GDP', 'Total Land', 'initial arable land', 'land fraction harvested', 'initial potentially arable land', 'potentially arable land total', 'processing loss', 'desired food ratio', 'IND OUT IN 1970', 'average life of agricultural inputs 1', 'average life of agricultural inputs 2', 'land yield factor 1', 'air pollution policy implementation time', 'average life of land normal', 'land life policy implementation time', 'urban and industrial land development time', 'initial urban and industrial land', 'initial land fertility', 'inherent land fertility', 'food shortage perception delay', 'subsistence food per capita', 'social discount', 'industrial output per capita desired', 'initial industrial capital', 'average life of industrial capital 1', 'average life of industrial capital 2', 'fraction of industrial output allocated to consumption constant 1', 'fraction of industrial output allocated to consumption constant 2', 'industrial capital output ratio 1', 'industrial equilibrium time', 'labor force participation fraction', 'labor utilization fraction delay time', 'average life of service capital 1', 'average life of service capital 2', 'service capital output ratio 1', 'service capital output ratio 2', 'initial service capital', 'FINAL TIME', 'INITIAL TIME', 'POLICY YEAR', 'TIME STEP', 'agricultural material toxicity index', 'assimilation half life in 1970', 'desired persistent pollution index', 'fraction of agricultural inputs from persistent materials', 'fraction of resources from persistent materials', 'industrial material toxicity index', 'industrial material emissions factor', 'persistent pollution generation factor 1', 'initial persistent pollution', 'persistent pollution in 1970', 'persistent pollution transmission delay', 'initial population 0 to 14', 'initial population 15 to 44', 'initial population 54 to 64', 'initial population 65 plus', 'desired completed family size normal', 'income expectation averaging time', 'lifetime perception delay', 'maximum total fertility normal', 'reproductive lifetime', 'social adjustment delay', 'fertility control effectiveness time', 'population equilibrium time', 'zero population growth time', 'THOUSAND', 'health services impact delay', 'life expectancy normal', 'desired resource use rate', 'initial nonrenewable resources', 'resource use factor 1', 'fraction of industrial capital allocated to obtaining resources switch time', 'technology development delay', 'PRICE OF FOOD']

# Paramètres (nombre de paramètres : d)
# On associe à chaque paramètre une loi de probabilité à définir...
param = {
    #'land_fraction_harvested': lambda: uniform(, 0.7, n),
    #'processing_loss': lambda: uniform(, 0.1, n),
    #'subsistence_food_per_capita': lambda: uniform(, 230, n),
    #'social_discount': lambda: uniform(, 0.07, n),
    #'fraction_of_agricultural_inputs_from_persistent_materials': lambda: uniform(, 0.001, n),
    #'fraction_of_resources_from_persistent_materials': lambda: uniform(, 0.02, n),
    #'industrial_material_emissions_factor': lambda: uniform(, 0.1, n),
    #'inherent_land_fertility': lambda: uniform(, 600, n),
    'urban_and_industrial_land_development_time': lambda: poisson(10, n),
    'food_shortage_perception_delay': lambda: poisson(2, n),
    'income_expectation_averaging_time': lambda: poisson(3, n),
    'labor_utilization_fraction_delay_time': lambda: poisson(2, n),
    'desired_food_ratio': lambda: uniform(1., 5., n),
    'industrial_output_per_capita_desired': lambda: uniform(1, 400*10, n),
    'labor_force_participation_fraction': lambda: uniform(0.001, 1., n),
    'reproductive_lifetime': lambda: uniform(12, 50, n),
    'desired_resource_use_rate': lambda: uniform(1, 4800000000.0*10, n),
    'price_of_food': lambda: uniform(0.001, 1., n),
    'lifetime_perception_delay': lambda: uniform(0, 200, n),
    'social_adjustment_delay': lambda: uniform(0, 200, n),
    'health_services_impact_delay': lambda: uniform(0, 200, n),
    'technology_development_delay': lambda: uniform(0, 200, n),
    'persistent_pollution_transmission_delay': lambda: uniform(0, 200, n),
    'fraction_of_industrial_capital_allocated_to_obtaining_resources_switch_time': lambda: uniform(1970, 2100, n),
    'air_pollution_policy_implementation_time': lambda: uniform(1970, 2100, n),
    'land_life_policy_implementation_time': lambda: uniform(1970, 2100, n),
    'fertility_control_effectiveness_time': lambda: uniform(1970, 2100, n),
    'population_equilibrium_time': lambda: uniform(1970, 2100, n),
    'zero_population_growth_time': lambda: uniform(1970, 2100, n),
    'industrial_equilibrium_time': lambda: uniform(1970, 2100, n),
    'policy_year': lambda: uniform(1970, 2100, n)
}

if __name__ == "__main__":
    '''
    Définition des fonctions
    '''
    # Sauvegarde à chaque signal (on ne peut pas handle le SIGKILL)
    def signal_handler(signal, frame):
        global outputs
        print('\nSignal:', signal)
        if len(outputs):
            flush_output()
        sys.exit(0)

    if hasattr(signal, 'SIGINT'):
        signal.signal(signal.SIGINT, signal_handler)
        print('SIGINT OK')
    if hasattr(signal, 'SIGUSR1'): signal.signal(signal.SIGUSR1, signal_handler)
    if hasattr(signal, 'SIGUSR2'): signal.signal(signal.SIGUSR2, signal_handler)
    if hasattr(signal, 'SIGTERM'): signal.signal(signal.SIGTERM, signal_handler)

    # Omission des warnings de division par zéro
    np.seterr(divide = 'ignore', invalid = 'ignore')

    # À la fin d'une exécution du modèle, on ajoute sa sortie à la suite des autres
    def append_output(output):
        print('.', end = "")
        sys.stdout.flush()
        outputs.append(output.drop(columns = const_params))

    # Sauvegarde des dernières exécutions depuis la dernière sauvegarde et vidage de la liste
    def flush_output():
        global outputs, total_run_length, current_timestamp
        
        filepath = outputs_path + 'runs_' + str(server_id) + '_' + str(int(current_timestamp)) + '.pkl'
        
        print()
        print('Nombre de runs depuis la dernière sauvegarde :', len(outputs))
        print('Taille de la sortie d\'un "run" :', outputs[0].shape)
        print('Vitesse moyenne d\'un run (s) :', str(total_run_length / len(outputs)))
        print('Sauvegarde dans', filepath)
        pkl_file = open(filepath, 'wb')
        pickle.dump(outputs, pkl_file)
        pkl_file.close()
        outputs = []

    # Modèle World3
    w3 = pysd.load(w3_path)

    # Framework d'analyse
    salt = Saltelli()

    '''
    Initialisation et démarrage des calculs
    '''
    while True:
        # Construction des plans d'expérience
        plan_1 = np.array([param[p_name]() for p_name in param.keys()]).T
        plan_2 = np.array([param[p_name]() for p_name in param.keys()]).T

        # Création des jeux d'entrées
        salt.generate_DoE(None, plan_1, plan_2, nboot = 100)
        print('Nombre de jeux d\'entrées :', len(salt.design))

        # Exécutions des modélisations
        outputs = []
        last_save_time = time.time()
        print('Timestamp de démarrage :', last_save_time)
        
        # Par défaut, on travail sur un seul coeur
        map_runner = map(lambda inputs: w3.run(inputs, reload = True), [dict(zip(param.keys(), jeu)) for jeu in salt.design])
        
        # On peut travailler sur plusieurs coeurs
        if multiproc:
            from multiprocessing_on_dill import Pool, cpu_count
            pool = Pool(cpu_count())
            map_runner = pool.imap_unordered(lambda inputs: w3.run(inputs, reload = True), [dict(zip(param.keys(), jeu)) for jeu in salt.design])
        
        for output in map_runner:
            
            append_output(output)
            current_timestamp = time.time()
            total_run_length = current_timestamp - last_save_time
            if total_run_length > time_between_saving:
                flush_output()
                last_save_time = current_timestamp
        
        flush_output()
        print('Fin de grande boucle')
