import numpy as np, pandas as pd
from Saltelli import Saltelli
from os import scandir
import sys
from w3_sensibility_analysis import param

'''
Configuration
'''
outputs_path = ''

# Correction du chemin des sorties si vide
if outputs_path == '':
    outputs_path = '.'

'''
Lecture des fichiers de sortie et concaténations en une seule dataframe
'''
outputs = []
print('Import des fichiers : ', end = "")
with scandir(outputs_path) as it:
    for entry in it:
        if entry.is_file() and entry.name.startswith('runs_'):
            print('.', end = '')

            output = pd.read_pickle(entry.name)
            outputs.extend(output)
print()

outputs_df = np.stack(outputs).astype(float)
print('Type de l\'array :', outputs_df.dtype)
print('Taille totale :', outputs_df.shape)

'''
Initialisation des variables
'''

d = len(param) # nombre de paramètres
runs_number, steps_number, vector_number = outputs_df.shape

print('Taille de s et t :', (d, 3, vector_number, steps_number))

s = np.zeros((d, 3, vector_number, steps_number)) # ordre 1
t = np.zeros((d, 3, vector_number, steps_number)) # ordre total

n_samples = int(runs_number / (d + 2)) # nombre d'échantillons par paramètre
useful_runs = int(runs_number / n_samples) * n_samples # suppression des paramètres en trop

if useful_runs != runs_number:
    outputs_df = outputs_df[:useful_runs, :, :]
    runs_number = useful_runs
'''
Réinstanciation du framework
'''
print('Nombre de paramètres d\'entrées à analyser :', len(param))
print('Nombre de runs utiles :', runs_number)

X1 = np.empty((n_samples, d))

salt = Saltelli()
salt.generate_DoE(None, X1, X1, nboot = 100)
'''
salt.dim = d # nombre de paramètres
salt.n = n_samples # nombre d'échantillons par paramètre
salt.nboot = 100
salt.conf = 0.95
salt.scheme = 'A'
'''

'''
Analyses
'''
progress = 1
for output_vector_n in range(vector_number): # boucle sur les vecteurs de sortie
    for step_k in range(steps_number): # boucle sur les pas de temps
        print(str(int(progress / (vector_number * steps_number) * 100)))
        sys.stdout.flush()
        salt.tell(outputs_df[:, step_k, output_vector_n])
        s[..., output_vector_n, step_k] = salt.S
        t[..., output_vector_n, step_k] = salt.T
        progress = progress + 1

'''
Enregistrement des fichiers
'''

# ID de la machine (pour nom des fichiers)
server_id = uuid4()

import pickle
pkl_file = open('analysis_s_' + str(uuid4()) + '.pkl')
pickle.dump(s, pkl_file)
pkl_file.close()

pkl_file = open('analysis_t_' + str(uuid4()) + '.pkl')
pickle.dump(t, pkl_file)
pkl_file.close()
