import dispy
# function 'compute' is distributed and executed with arguments
# supplied with 'cluster.submit' below
def compute(n):
    '''
    import time, socket
    time.sleep(n)
    host = socket.gethostname()
    return (host, n)
    '''
    
    import pysd
    import urllib.request

    urllib.request.urlretrieve ("https://gitlab.inria.fr/arasoldi/world3-03_python/raw/master/world3.py", "world3.py")
    w3 = pysd.load('world3.py')
    #stocks = w3.run()
    
    import dill
    dill.dump(w3, open('w3.pkl', 'wb'))
    return True

if __name__ == '__main__':
    # executed on client only; variables created below, including modules imported,
    # are not available in job computations
    import dispy, random
    # distribute 'compute' to nodes; in this case, 'compute' does not have
    # any dependencies to run on nodes
    cluster = dispy.JobCluster(compute)
    # run 'compute' with 20 random numbers on available CPUs
    jobs = []
    for i in range(20):
        job = cluster.submit(random.randint(5,20))
        job.id = i # associate an ID to identify jobs (if needed later)
        jobs.append(job)
    # cluster.wait() # waits until all jobs finish
    for job in jobs:
        '''
        host, n = job() # waits for job to finish and returns results
        print('%s executed job %s at %s with %s' % (host, job.id, job.start_time, n))
        '''
        e = job()
        print(e)
        # other fields of 'job' that may be useful:
        # job.stdout, job.stderr, job.exception, job.ip_addr, job.end_time
    cluster.print_status()  # shows which nodes executed how many jobs etc.

    import pysd
    w3 = pysd.load('../world3.py')
    stocks = w3.run()
    population_in_1900 = stocks['population'][1900]
    print(population_in_1900)
