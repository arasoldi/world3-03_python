@cache('run')
def desired_resource_use_rate():
    """
    desired resource use rate

    Resource units/year

    constant

    Desired non-renewable resource usage rate (DNRUR#--)
    """
    return 4.8e+09


@cache('step')
def fraction_of_resources_remaining():
    """
    fraction of resources remaining

    Dmnl

    component

    Non-renewable resource fraction remaining (NRFR#133).
    """
    return nonrenewable_resources() / initial_nonrenewable_resources()


@cache('step')
def resource_usage_rate():
    """
    resource usage rate

    Resource units/year

    component

    Non-renewable resource use rate (NRUR#130).
    """
    return (((population())) * (per_capita_resource_use_multiplier())) * (resource_use_factor())


@cache('run')
def initial_nonrenewable_resources():
    """
    initial nonrenewable resources

    Resource units

    constant

    NONRENEWABLE RESOURCE INITIAL (NR#129.2).
    """
    return 1e+12


@cache('step')
def nonrenewable_resources():
    """
    Nonrenewable Resources

    Resource units

    component

    Non-renewable resource (NR#129)
    """
    return integ_nonrenewable_resources()

integ_nonrenewable_resources = functions.Integ(lambda: (-resource_usage_rate()),
                                               lambda: initial_nonrenewable_resources())

@cache('step')
def fraction_of_capital_allocated_to_obtaining_resources_1():
    """
    fraction of capital allocated to obtaining resources 1

    Dmnl

    component

    Fraction of capital allocated to obtaining resources before switch time (FCAOR1#135).
    """
    return fraction_of_capital_allocated_to_obtaining_resources_1_table(
        fraction_of_resources_remaining())


def fraction_of_capital_allocated_to_obtaining_resources_1_table(x):
    """
    fraction of capital allocated to obtaining resources 1 table

    Dmnl

    lookup

    Table relating the fraction of resources remaining to capital allocated to resource extraction (FCAOR1T#135.1).
    """
    return functions.lookup(x, [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
                            [1, 0.9, 0.7, 0.5, 0.2, 0.1, 0.05, 0.05, 0.05, 0.05, 0.05])


@cache('step')
def fraction_of_capital_allocated_to_obtaining_resources_2():
    """
    fraction of capital allocated to obtaining resources 2

    Dmnl

    component

    Fraction of capital allocated to obtaining resources after switch time (FCAOR2#136).
    """
    return fraction_of_capital_allocated_to_obtaining_resources_2_table(
        fraction_of_resources_remaining())


def fraction_of_capital_allocated_to_obtaining_resources_2_table(x):
    """
    fraction of capital allocated to obtaining resources 2 table

    Dmnl

    lookup

    Table relating the fraction of resources remaining to capital allocated to resource extraction (FCAOR2T#136.1).
    """
    return functions.lookup(x, [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
                            [1, 0.2, 0.1, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05])


@cache('run')
def resource_use_factor_1():
    """
    resource use factor 1

    Dmnl

    constant

    The nonrenewable resource usage factor before the policy year (NRUF1#131.1).
    """
    return 1


@cache('step')
def resource_use_fact_2():
    """
    resource use fact 2

    Dmnl

    component

    The nonrenewable resource usage factor after the policy year (NRUF2#131.2).
    """
    return smooth_resource_conservation_technology_technology_development_delay_resource_conservation_technology_3()

smooth_resource_conservation_technology_technology_development_delay_resource_conservation_technology_3 = functions.Smooth(
    lambda: resource_conservation_technology(), lambda: technology_development_delay(),
    lambda: resource_conservation_technology(), lambda: 3)

@cache('step')
def resource_technology_change_rate_multiplier():
    """
    resource technology change rate multiplier

    Dmnl/year

    component

    Resource technology change multiplier (NRCM#--)
    """
    return resource_technology_change_mult_table(
        1 - resource_usage_rate() / desired_resource_use_rate())


def resource_technology_change_mult_table(x):
    """
    resource technology change mult table

    Dmnl/year

    lookup

    Table relating resource use to technological change. (NRCMT#--)
    """
    return functions.lookup(x, [-1, 0], [0, 0])


@cache('step')
def per_capita_resource_use_multiplier():
    """
    per capita resource use multiplier

    Resource unit/(Person*year)

    component

    Per capita resource usage multiplier (PCRUM#132).
    """
    return per_capita_resource_use_mult_table(industrial_output_per_capita() / gdp_pc_unit())


def per_capita_resource_use_mult_table(x):
    """
    per capita resource use mult table

    Resource units/(Person*year)

    lookup

    Table relating industrial output to resource usage per capita (PCRUMT#132.1).
    """
    return functions.lookup(x, [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600],
                            [0, 0.85, 2.6, 3.4, 3.8, 4.1, 4.4, 4.7, 5])


@cache('step')
def resource_conservation_technology():
    """
    Resource Conservation Technology

    Dmnl

    component

    Non-renewable resource technology (NRTD#--)
    """
    return integ_resource_conservation_technology()

integ_resource_conservation_technology = functions.Integ(lambda: resource_technology_change_rate(),
                                                         lambda: 1)

@cache('step')
def resource_technology_change_rate():
    """
    resource technology change rate

    1/year

    component

    RESOURCE TECHNOLOGY IMPROVEMENT RATE (NRATE-##).
    """
    return functions.if_then_else(
        time() >= policy_year(),
        resource_conservation_technology() * resource_technology_change_rate_multiplier(), 0)


@cache('step')
def fraction_of_industrial_capital_allocated_to_obtaining_resources():
    """
    fraction of industrial capital allocated to obtaining resources

    Dmnl

    component

    FRACTION OF CAPITAL ALLOCATED TO OBTAINING RESOURCES (FCAOR#134).
    """
    return functions.if_then_else(
        time() >= fraction_of_industrial_capital_allocated_to_obtaining_resources_switch_time(),
        fraction_of_capital_allocated_to_obtaining_resources_2(),
        fraction_of_capital_allocated_to_obtaining_resources_1())


@cache('step')
def resource_use_factor():
    """
    resource use factor

    Dmnl

    component

    NONRENEWABLE RESOURCE USAGE FACTOR (NRUF#131).
    """
    return functions.if_then_else(time() >= policy_year(), resource_use_fact_2(),
                                  resource_use_factor_1())


@cache('run')
def fraction_of_industrial_capital_allocated_to_obtaining_resources_switch_time():
    """
    fraction of industrial capital allocated to obtaining resources switch time

    year

    constant

    Time at which to switch between alternative fraction of capital alocated to obtaining resources (FCAORTM#--).
    """
    return 4000


@cache('run')
def technology_development_delay():
    """
    technology development delay

    year

    constant

    The technology development delay (TDD#--)
    """
    return 20


