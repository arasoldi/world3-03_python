@cache('step')
def deaths_0_to_14():
    """
    deaths 0 to 14

    Person/year

    component

    The number of deaths per year among people 0 to 14 years of age (D1#3).
    """
    return population_0_to_14() * mortality_0_to_14()


@cache('step')
def deaths_15_to_44():
    """
    deaths 15 to 44

    Person/year

    component

    The number of deaths per year among people 15 to 44 years of age (D2#7).
    """
    return population_15_to_44() * mortality_15_to_44()


@cache('step')
def deaths_45_to_64():
    """
    deaths 45 to 64

    Person/year

    component

    The number of deaths per year among people 55 to 64 years of age (D3#11).
    """
    return population_45_to_64() * mortality_45_to_64()


@cache('step')
def deaths_65_plus():
    """
    deaths 65 plus

    Person/year

    component

    The number of deaths per year among people 65 and older (D4#15).
    """
    return population_65_plus() * mortality_65_plus()


@cache('step')
def maturation_14_to_15():
    """
    maturation 14 to 15

    Person/year

    component

    The fractional rate at which people aged 0-14 mature into the next age cohort (MAT1#5).
    """
    return ((population_0_to_14())) * (1 - mortality_0_to_14()) / 15


@cache('step')
def maturation_44_to_45():
    """
    maturation 44 to 45

    Person/year

    component

    The fractional rate at which people aged 15-44 mature into the next age cohort (MAT2#9).
    """
    return ((population_15_to_44())) * (1 - mortality_15_to_44()) / 30


@cache('step')
def maturation_64_to_65():
    """
    maturation 64 to 65

    Person/year

    component

    The fractional rate at which people aged 45-64 mature into the next age cohort (MAT3#13).
    """
    return ((population_45_to_64())) * (1 - mortality_45_to_64()) / 20


@cache('step')
def mortality_45_to_64():
    """
    mortality 45 to 64

    1/year

    component

    The fractional mortality rate for people aged 45-64 (M3#12).
    """
    return mortality_45_to_64_table(life_expectancy() / one_year())


def mortality_45_to_64_table(x):
    """
    mortality 45 to 64 table

    1/year

    lookup

    The table relating average life to mortality in the 45 to 64 age group (M2T#12.1).
    """
    return functions.lookup(x, [20, 30, 40, 50, 60, 70, 80],
                            [0.0562, 0.0373, 0.0252, 0.0171, 0.0118, 0.0083, 0.006])


@cache('step')
def mortality_65_plus():
    """
    mortality 65 plus

    1/year

    component

    The fractional mortality rate for people over 65 (M4#16).
    """
    return mortality_65_plus_table(life_expectancy() / one_year())


def mortality_65_plus_table(x):
    """
    mortality 65 plus table

    1/year

    lookup

    The table relating average life expectancy to mortality among people over 65 (M4T#16.1)
    """
    return functions.lookup(x, [20, 30, 40, 50, 60, 70, 80],
                            [0.13, 0.11, 0.09, 0.07, 0.06, 0.05, 0.04])


@cache('step')
def mortality_0_to_14():
    """
    mortality 0 to 14

    1/year

    component

    The fractional mortality rate for people aged 0-14 (M1#4).
    """
    return mortality_0_to_14_table(life_expectancy() / one_year())


def mortality_0_to_14_table(x):
    """
    mortality 0 to 14 table

    1/year

    lookup

    The table relating average life to mortality in the 0 to 14 age group (M1T#4.1).
    """
    return functions.lookup(x, [20, 30, 40, 50, 60, 70, 80],
                            [0.0567, 0.0366, 0.0243, 0.0155, 0.0082, 0.0023, 0.001])


@cache('step')
def mortality_15_to_44():
    """
    mortality 15 to 44

    1/year

    component

    The fractional mortality rate for people aged 15-44 (M2#8).
    """
    return mortality_15_to_44_table(life_expectancy() / one_year())


def mortality_15_to_44_table(x):
    """
    mortality 15 to 44 table

    1/year

    lookup

    The table relating average life to mortality in the 15 to 44 age group (M2T#8.1).
    """
    return functions.lookup(x, [20, 30, 40, 50, 60, 70, 80],
                            [0.0266, 0.0171, 0.011, 0.0065, 0.004, 0.0016, 0.0008])


@cache('step')
def population_0_to_14():
    """
    Population 0 To 14

    Person

    component

    World population, AGES 0-14 (P1#2)
    """
    return integ_population_0_to_14()

integ_population_0_to_14 = functions.Integ(
    lambda: (births() - deaths_0_to_14() - maturation_14_to_15()),
    lambda: initial_population_0_to_14())

@cache('run')
def initial_population_0_to_14():
    """
    initial population 0 to 14

    Person

    constant

    The initial number of people aged 0 to 14 (P1I#2.2).
    """
    return 6.5e+08


@cache('step')
def population_15_to_44():
    """
    Population 15 To 44

    Person

    component

    World population, AGES 15-44 (P2#6)
    """
    return integ_population_15_to_44()

integ_population_15_to_44 = functions.Integ(
    lambda: (maturation_14_to_15() - deaths_15_to_44() - maturation_44_to_45()),
    lambda: initial_population_15_to_44())

@cache('run')
def initial_population_15_to_44():
    """
    initial population 15 to 44

    Person

    constant

    The initial number of people aged 15 to 44 (P2I#6.2).
    """
    return 7e+08


@cache('step')
def population_45_to_64():
    """
    Population 45 To 64

    Person

    component

    The world population aged 0 to 14 (P3#10).
    """
    return integ_population_45_to_64()

integ_population_45_to_64 = functions.Integ(
    lambda: (maturation_44_to_45() - deaths_45_to_64() - maturation_64_to_65()),
    lambda: initial_population_54_to_64())
    
@cache('run')
def initial_population_54_to_64():
    """
    initial population 54 to 64

    Person

    constant

    The initial number of people aged 45 to 64 (P3I#10.2).
    """
    return 1.9e+08


@cache('step')
def population_65_plus():
    """
    Population 65 Plus

    Person

    component

    The world population aged 65 and over (P4#14).
    """
    return integ_population_65_plus()

integ_population_65_plus = functions.Integ(lambda: (maturation_64_to_65() - deaths_65_plus()),
                                           lambda: initial_population_65_plus())

@cache('run')
def initial_population_65_plus():
    """
    initial population 65 plus

    Person

    constant

    The initial number of people aged 65 and over (P4I#14.2)
    """
    return 6e+07


@cache('step')
def population():
    """
    population

    Person

    component

    Total world population in all age groups (POP#1).
    """
    return population_0_to_14() + population_15_to_44() + population_45_to_64(
    ) + population_65_plus()


