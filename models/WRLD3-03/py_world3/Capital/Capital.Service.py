@cache('run')
def average_life_of_service_capital_1():
    """
    average life of service capital 1

    year

    constant

    Average lifetime of service capital before policy time (ALSC1#69.1).
    """
    return 20


@cache('run')
def average_life_of_service_capital_2():
    """
    average life of service capital 2

    year

    constant

    Average lifetime of service capital after policy time (ALSC2#69.2).
    """
    return 20


@cache('step')
def fraction_of_industrial_output_allocated_to_services_1():
    """
    fraction of industrial output allocated to services 1

    Dmnl

    component

    FRACTION OF INDUSTRIAL OUTPUT ALLOCATED TO SERVICES before policy year (FIOAS1#64).
    """
    return fraction_of_industrial_output_allocated_to_services_table_1(
        service_output_per_capita() / indicated_services_output_per_capita())


def fraction_of_industrial_output_allocated_to_services_table_1(x):
    """
    fraction of industrial output allocated to services table 1

    Dmnl

    lookup

    Table relating service output to the fraction of industrial output allocated to service (FIOAS1T#64.1).
    """
    return functions.lookup(x, [0, 0.5, 1, 1.5, 2], [0.3, 0.2, 0.1, 0.05, 0])


@cache('step')
def fraction_of_industrial_output_allocated_to_services_2():
    """
    fraction of industrial output allocated to services 2

    Dmnl

    component

    FRACTION OF INDUSTRIAL OUTPUT ALLOCATED TO SERVICES after policy year (FIOAS2#65).
    """
    return fraction_of_industrial_output_allocated_to_services_table_2(
        service_output_per_capita() / indicated_services_output_per_capita())


def fraction_of_industrial_output_allocated_to_services_table_2(x):
    """
    fraction of industrial output allocated to services table 2

    Dmnl

    lookup

    Table relating service output to the fraction of industrial output allocated to service (FIOAS2T#65.1).
    """
    return functions.lookup(x, [0, 0.5, 1, 1.5, 2], [0.3, 0.2, 0.1, 0.05, 0])


@cache('step')
def indicated_services_output_per_capita_1():
    """
    indicated services output per capita 1

    $/(Person*year)

    component

    Indicated service output per capita before policy year (ISOPC1#61).
    """
    return indicated_services_output_per_capita_table_1(
        industrial_output_per_capita() / gdp_pc_unit())


def indicated_services_output_per_capita_table_1(x):
    """
    indicated services output per capita table 1

    $/(Person*year)

    lookup

    Table relating industrial output per capita to the indicated service output per capita before policy year (ISOPC1T#61.1).
    """
    return functions.lookup(x, [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600],
                            [40, 300, 640, 1000, 1220, 1450, 1650, 1800, 2000])


@cache('step')
def indicated_services_output_per_capita_2():
    """
    indicated services output per capita 2

    $/(Person*year)

    component

    Indicated service output per capita after policy year (ISOPC2#621).
    """
    return indicated_services_output_per_capita_table_2(
        industrial_output_per_capita() / gdp_pc_unit())


def indicated_services_output_per_capita_table_2(x):
    """
    indicated services output per capita table 2

    $/(Person*year)

    lookup

    Table relating industrial output per capita to the indicated service output per capita afte policy year (ISOPC1T#62.1).
    """
    return functions.lookup(x, [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600],
                            [40, 300, 640, 1000, 1220, 1450, 1650, 1800, 2000])


@cache('run')
def service_capital_output_ratio_1():
    """
    service capital output ratio 1

    year

    constant

    Service capital output ratio before policy year (SCOR1#72.1).
    """
    return 1


@cache('run')
def service_capital_output_ratio_2():
    """
    service capital output ratio 2

    year

    constant

    Service capital output ratio after policy year (SCOR1#72.1).
    """
    return 1


@cache('step')
def average_life_of_service_capital():
    """
    average life of service capital

    year

    component

    AVERAGE LIFETIME OF SERVICE CAPITAL (ALSC#69)
    """
    return functions.if_then_else(time() >= policy_year(), average_life_of_service_capital_2(),
                                  average_life_of_service_capital_1())


@cache('step')
def fraction_of_industrial_output_allocated_to_services():
    """
    fraction of industrial output allocated to services

    Dmnl

    component

    FRACTION OF INDUSTRIAL OUTPUT ALLOCATED TO SERVICES (FIOAS#63).
    """
    return functions.if_then_else(time() >= policy_year(),
                                  fraction_of_industrial_output_allocated_to_services_2(),
                                  fraction_of_industrial_output_allocated_to_services_1())


@cache('step')
def indicated_services_output_per_capita():
    """
    indicated services output per capita

    $/(Person*year)

    component

    Indicated service output per capita (ISOPC#60).
    """
    return functions.if_then_else(time() >= policy_year(),
                                  indicated_services_output_per_capita_2(),
                                  indicated_services_output_per_capita_1())


@cache('step')
def service_capital_output_ratio():
    """
    service capital output ratio

    year

    component

    Service capital output ratio (SCOR#72).
    """
    return functions.if_then_else(time() >= policy_year(), service_capital_output_ratio_2(),
                                  service_capital_output_ratio_1())


@cache('step')
def service_capital_depreciation():
    """
    service capital depreciation

    $/year

    component

    SERVICE CAPITAL DEPRECIATION RATE (SCDR#68).
    """
    return service_capital() / average_life_of_service_capital()


@cache('step')
def service_capital_investment():
    """
    service capital investment

    $/year

    component

    SERVICE CAPITAL INVESTMENT RATE (SCIR#66).
    """
    return ((industrial_output())) * (fraction_of_industrial_output_allocated_to_services())


@cache('step')
def service_output_per_capita():
    """
    service output per capita

    $/(Person*year)

    component

    SERVICE OUTPUT PER CAPITA (SOPC#71).
    """
    return service_output() / population()


@cache('step')
def service_capital():
    """
    Service Capital

    $

    component

    Service capital (SC#67).
    """
    return integ_service_capital()

integ_service_capital = functions.Integ(
    lambda: (service_capital_investment() - service_capital_depreciation()),
    lambda: initial_service_capital())

@cache('run')
def initial_service_capital():
    """
    initial service capital

    $

    constant

    The initial level of service capital (SCI#67.2)
    """
    return 1.44e+11


@cache('step')
def service_output():
    """
    service output

    $/year

    component

    Service output (SO#70).
    """
    return (
        (service_capital())) * (capacity_utilization_fraction()) / service_capital_output_ratio()


