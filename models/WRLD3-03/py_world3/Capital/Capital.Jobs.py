@cache('step')
def delayed_labor_utilization_fraction():
    """
    Delayed Labor Utilization Fraction

    Dmnl

    component

    LABOR UTILIZATION FRACTION DELAYED (LUFD#82)
    """
    return smooth_labor_utilization_fraction_labor_utilization_fraction_delay_time_1_1()


smooth_labor_utilization_fraction_labor_utilization_fraction_delay_time_1_1 = functions.Smooth(
    lambda: labor_utilization_fraction(), lambda: labor_utilization_fraction_delay_time(),
    lambda: 1, lambda: 1)

@cache('step')
def capacity_utilization_fraction():
    """
    capacity utilization fraction

    Dmnl

    component

    CAPITAL UTILIZATION FRACTION (CUF#83)
    """
    return functions.active_initial(
        capacity_utilization_fraction_table(delayed_labor_utilization_fraction()), 1)


def capacity_utilization_fraction_table(x):
    """
    capacity utilization fraction table

    Dmnl

    lookup

    Table relating labor utilization to capacity utilization (CUFT#83.2).
    """
    return functions.lookup(x, [1, 3, 5, 7, 9, 11], [1, 0.9, 0.7, 0.3, 0.1, 0.1])


@cache('step')
def jobs():
    """
    jobs

    Person

    component

    JOBS (J#73).
    """
    return potential_jobs_industrial_sector() + potential_jobs_agricultural_sector(
    ) + potential_jobs_service_sector()


@cache('step')
def jobs_per_hectare():
    """
    jobs per hectare

    Person/hectare

    component

    Jobs per hectare in agriculture (JPH#79).
    """
    return jobs_per_hectare_table(agricultural_input_per_hectare() / unit_agricultural_input())


def jobs_per_hectare_table(x):
    """
    jobs per hectare table

    Person/hectare

    lookup

    Table relating agricultural input intensity to the number of jobs per hectare in agriculture (JPHT#79.1).
    """
    return functions.lookup(x, [2, 6, 10, 14, 18, 22, 26, 30],
                            [2, 0.5, 0.4, 0.3, 0.27, 0.24, 0.2, 0.2])


@cache('step')
def jobs_per_industrial_capital_unit():
    """
    jobs per industrial capital unit

    Person/$

    component

    Jobs per industrial capital units (JPICU#75).
    """
    return (jobs_per_industrial_capital_unit_table(
        industrial_output_per_capita() / gdp_pc_unit())) * 0.001


def jobs_per_industrial_capital_unit_table(x):
    """
    jobs per industrial capital unit table

    Person/$

    lookup

    Table relating industrial output per capita to job per industrial capital unit (JPICUT#75.1).
    """
    return functions.lookup(x, [50, 200, 350, 500, 650, 800], [0.37, 0.18, 0.12, 0.09, 0.07, 0.06])


@cache('step')
def jobs_per_service_capital_unit():
    """
    jobs per service capital unit

    Person/$

    component

    Jobs per service capital unit (JPSCU#77).
    """
    return (jobs_per_service_capital_unit_table(
        service_output_per_capita() / gdp_pc_unit())) * 0.001


def jobs_per_service_capital_unit_table(x):
    """
    jobs per service capital unit table

    Person/$

    lookup

    Table relating service output per capita to job per service capital unit (JPICUT#77.1).
    """
    return functions.lookup(x, [50, 200, 350, 500, 650, 800], [1.1, 0.6, 0.35, 0.2, 0.15, 0.15])


@cache('step')
def labor_force():
    """
    labor force

    Person

    component

    LABOR FORCE (LF#80).
    """
    return (population_15_to_44() + population_45_to_64()) * labor_force_participation_fraction()


@cache('run')
def labor_force_participation_fraction():
    """
    labor force participation fraction

    Dmnl

    constant

    LABOR FORCE PARTICIPATION FRACTION (LFPF#80.1)
    """
    return 0.75


@cache('step')
def labor_utilization_fraction():
    """
    labor utilization fraction

    Dmnl

    component

    Labor utilization fraction (LUF#81).
    """
    return jobs() / labor_force()


@cache('run')
def labor_utilization_fraction_delay_time():
    """
    labor utilization fraction delay time

    year

    constant

    Labor utilization fraction delay time (LUFDT#82.1)
    """
    return 2


@cache('step')
def potential_jobs_agricultural_sector():
    """
    potential jobs agricultural sector

    Person

    component

    Potential jobs in the agricultural sector (PJAS#78).
    """
    return ((jobs_per_hectare())) * (arable_land())


@cache('step')
def potential_jobs_industrial_sector():
    """
    potential jobs industrial sector

    Person

    component

    POTENTIAL JOBS IN INDUSTRIAL SECTOR (PKIS#74).
    """
    return industrial_capital() * jobs_per_industrial_capital_unit()


@cache('step')
def potential_jobs_service_sector():
    """
    potential jobs service sector

    Person

    component

    Potential jobs in the service sector (PJSS#76).
    """
    return ((service_capital())) * (jobs_per_service_capital_unit())


