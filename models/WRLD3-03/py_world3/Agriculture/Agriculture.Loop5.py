@cache('run')
def inherent_land_fertility():
    """
    inherent land fertility

    Veg eq kg/(year*hectare)

    constant

    INHERENT LAND FERTILITY (ILF#124.1).
    """
    return 600


@cache('step')
def land_fertility_regeneration():
    """
    land fertility regeneration

    Veg eq kg/(year*year*hectare)

    component

    Land fertility regeneration (LFR#124).
    """
    return (inherent_land_fertility() - land_fertility()) / land_fertility_regeneration_time()


@cache('step')
def land_fertility_regeneration_time():
    """
    land fertility regeneration time

    year

    component

    LAND FERTILITY REGENERATION TIME (LFRT#125)
    """
    return land_fertility_regeneration_time_table(
        fraction_of_agricultural_inputs_for_land_maintenance())


def land_fertility_regeneration_time_table(x):
    """
    land fertility regeneration time table

    year

    lookup

    Table relating inputs to land maintenance to land fertility regeneration (LFRTT#125.1).
    """
    return functions.lookup(x, [0, 0.02, 0.04, 0.06, 0.08, 0.1], [20, 13, 8, 4, 2, 2])


