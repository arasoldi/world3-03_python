@cache('step')
def agricultural_inputs():
    """
    Agricultural Inputs

    $/year

    component

    AGRICULTURAL INPUTS (AI#99)
    """
    return smooth_current_agricultural_inputs_average_life_agricultural_inputs_current_agricultural_inputs_1()

#Circular init!
smooth_current_agricultural_inputs_average_life_agricultural_inputs_current_agricultural_inputs_1 = functions.Smooth(
    lambda: current_agricultural_inputs(), lambda: average_life_agricultural_inputs(),
    lambda: 5e+09, lambda: 1)
    
@cache('step')
def average_life_agricultural_inputs():
    """
    average life agricultural inputs

    year

    component

    AVERAGE LIFETIME OF AGRICULTURAL INPUTS (ALAI#100)
    """
    return functions.if_then_else(time() >= policy_year(), average_life_of_agricultural_inputs_2(),
                                  average_life_of_agricultural_inputs_1())


@cache('step')
def agricultural_input_per_hectare():
    """
    agricultural input per hectare

    $/(year*hectare)

    component

    AGRICULTURAL INPUTS PER HECTARE (AIPH#101)
    """
    return agricultural_inputs() * (
        1 - fraction_of_agricultural_inputs_for_land_maintenance()) / arable_land()


@cache('step')
def current_agricultural_inputs():
    """
    current agricultural inputs

    $/year

    component

    CURRENT AGRICULTURAL INPUTS (CAI#98).
    """
    return functions.active_initial(
        total_agricultural_investment() *
        (1 - fraction_of_agricultural_inputs_allocated_to_land_development()), 5e+09)


@cache('run')
def desired_food_ratio():
    """
    desired food ratio

    Dmnl

    constant

    desired food ratio (DFR#--)
    """
    return 2


@cache('run')
def ind_out_in_1970():
    """
    IND OUT IN 1970

    $/year

    constant

    INDUSTRIAL OUTPUT IN 1970 (IO70#107.2)
    """
    return 7.9e+11


@cache('step')
def land_yield():
    """
    land yield

    Veg eq kg/(year*hectare)

    component

    LAND YIELD (LY#103)
    """
    return land_yield_multiplier_from_technology() * land_fertility(
    ) * land_yield_multiplier_from_capital() * land_yield_multiplier_from_air_pollution()


@cache('step')
def land_yield_multiplier_from_capital():
    """
    land yield multiplier from capital

    Dmnl

    component

    LAND YIELD MULTIPLIER FROM CAPITAL (LYMC#102)
    """
    return land_yield_multiplier_from_capital_table(
        agricultural_input_per_hectare() / unit_agricultural_input())


def land_yield_multiplier_from_capital_table(x):
    """
    land yield multiplier from capital table

    Dmnl

    lookup

    Table relating agricultural inputs to land yeild (LYMCT#102.1).
    """
    return functions.lookup(x, [
        0, 40, 80, 120, 160, 200, 240, 280, 320, 360, 400, 440, 480, 520, 560, 600, 640, 680, 720,
        760, 800, 840, 880, 920, 960, 1000
    ], [
        1, 3, 4.5, 5, 5.3, 5.6, 5.9, 6.1, 6.35, 6.6, 6.9, 7.2, 7.4, 7.6, 7.8, 8, 8.2, 8.4, 8.6,
        8.8, 9, 9.2, 9.4, 9.6, 9.8, 10
    ])


@cache('run')
def average_life_of_agricultural_inputs_1():
    """
    average life of agricultural inputs 1

    year

    constant

    The average life of agricultural inputs before policy time (ALAI1#100.1)
    """
    return 2


@cache('run')
def average_life_of_agricultural_inputs_2():
    """
    average life of agricultural inputs 2

    year

    constant

    The average life of agricultural inputs after policy time (ALAI2#100.2)
    """
    return 2


@cache('run')
def land_yield_factor_1():
    """
    land yield factor 1

    Dmnl

    constant

    Land yield factor before policy year (LYF1#104.1).
    """
    return 1


@cache('step')
def land_yield_factor_2():
    """
    land yield factor 2

    Dmnl

    component

    Land yield factor after policy year (LYF1#104.2).
    """
    return smooth_land_yield_technology_technology_development_delay_land_yield_technology_3()



smooth_land_yield_technology_technology_development_delay_land_yield_technology_3 = functions.Smooth(
    lambda: land_yield_technology(), lambda: technology_development_delay(),
    lambda: land_yield_technology(), lambda: 3)
    
@cache('step')
def land_yield_multipler_from_air_pollution_1():
    """
    land yield multipler from air pollution 1

    Dmnl

    component

    Land yield multiplier from air pollution before air poll time (LYMAP1#106).
    """
    return land_yield_multipler_from_air_pollution_table_1(industrial_output() / ind_out_in_1970())


def land_yield_multipler_from_air_pollution_table_1(x):
    """
    land yield multipler from air pollution table 1

    Dmnl

    lookup

    Table relating non-persistent pollution from industry to agricultural output (LYMAPT#106.1).
    """
    return functions.lookup(x, [0, 10, 20, 30], [1, 1, 0.7, 0.4])


@cache('step')
def land_yield_multiplier_from_air_pollution_2():
    """
    land yield multiplier from air pollution 2

    Dmnl

    component

    Land yield multiplier from air pollution after air poll time (LYMAP2#107).
    """
    return land_yield_multipler_from_air_pollution_table_2(industrial_output() / ind_out_in_1970())


def land_yield_multipler_from_air_pollution_table_2(x):
    """
    land yield multipler from air pollution table 2

    Dmnl

    lookup

    Table relating non-persistent pollution from industry to agricultural output (LYMAPT#107.1).
    """
    return functions.lookup(x, [0, 10, 20, 30], [1, 1, 0.98, 0.95])


@cache('step')
def land_yield_technology_change_rate_multiplier():
    """
    land yield technology change rate multiplier

    1/year

    component

    Land yield from technology change multiplier (LYCM#--)
    """
    return land_yield_technology_change_rate_multiplier_table(desired_food_ratio() - food_ratio())


def land_yield_technology_change_rate_multiplier_table(x):
    """
    land yield technology change rate multiplier table

    1/year

    lookup

    Table relating the food ratio gap to the change in agricultural technology (LYCMT#--).
    """
    return functions.lookup(x, [0, 1], [0, 0])


@cache('step')
def land_yield_multiplier_from_technology():
    """
    land yield multiplier from technology

    Dmnl

    component

    Land Yield factor (LYF#104)
    """
    return functions.if_then_else(time() >= policy_year(), land_yield_factor_2(),
                                  land_yield_factor_1())


@cache('step')
def land_yield_multiplier_from_air_pollution():
    """
    land yield multiplier from air pollution

    Dmnl

    component

    Land yield multiplier from air pollution (LYMAP#105).
    """
    return functions.if_then_else(time() >= air_pollution_policy_implementation_time(),
                                  land_yield_multiplier_from_air_pollution_2(),
                                  land_yield_multipler_from_air_pollution_1())


@cache('run')
def air_pollution_policy_implementation_time():
    """
    air pollution policy implementation time

    year

    constant

    Air Pollution switch time (ARPTM#--)
    """
    return 4000


@cache('step')
def land_yield_technology():
    """
    Land Yield Technology

    Dmnl

    component

    LAND YIELD TECHNOLOGY INITIATED (LYTD#--)
    """
    return integ_land_yield_technology()


integ_land_yield_technology = functions.Integ(lambda: land_yield_technology_change_rate(),
                                              lambda: 1)

@cache('step')
def land_yield_technology_change_rate():
    """
    land yield technology change rate

    1/year

    component

    Land yield from technology change rate (LYTDR#--)
    """
    return functions.if_then_else(
        time() >= policy_year(),
        land_yield_technology() * land_yield_technology_change_rate_multiplier(), 0)


