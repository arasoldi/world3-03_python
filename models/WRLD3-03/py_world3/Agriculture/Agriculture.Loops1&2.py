@cache('step')
def fraction_of_agricultural_inputs_allocated_to_land_development():
    """
    fraction of agricultural inputs allocated to land development

    Dmnl

    component

    Fraction of inputs allocated to land devlelopment (FIALD#108).
    """
    return fraction_of_agricultural_inputs_allocated_to_land_development_table(
        (marginal_productivity_of_land_development() /
         marginal_productivity_of_agricultural_inputs()))


def fraction_of_agricultural_inputs_allocated_to_land_development_table(x):
    """
    fraction of agricultural inputs allocated to land development table

    Dmnl

    lookup

    Table relating the marginal productivity of land to the fraction of inputs allocated to new land development (FIALDT#108.1).
    """
    return functions.lookup(x, [0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2],
                            [0, 0.05, 0.15, 0.3, 0.5, 0.7, 0.85, 0.95, 1])


@cache('step')
def marginal_land_yield_multiplier_from_capital():
    """
    marginal land yield multiplier from capital

    hectare/$

    component

    MARGINAL LAND YIELD MULTIPLIER FROM CAPITAL (MLYMC#111).
    """
    return marginal_land_yield_multiplier_from_capital_table(
        agricultural_input_per_hectare() / unit_agricultural_input())


def marginal_land_yield_multiplier_from_capital_table(x):
    """
    marginal land yield multiplier from capital table

    hectare/$

    lookup

    Table relating agricultural inputs to marginal land yield (MLYMCT#111.1).
    """
    return functions.lookup(
        x, [0, 40, 80, 120, 160, 200, 240, 280, 320, 360, 400, 440, 480, 520, 560, 600], [
            0.075, 0.03, 0.015, 0.011, 0.009, 0.008, 0.007, 0.006, 0.005, 0.005, 0.005, 0.005,
            0.005, 0.005, 0.005, 0.005
        ])


@cache('step')
def marginal_productivity_of_agricultural_inputs():
    """
    marginal productivity of agricultural inputs

    Veg eq kg/$

    component

    MARGINAL PRODUCTIVITY OF AGRICULTURAL INPUTS (MPAI#110).
    """
    return average_life_agricultural_inputs() * land_yield(
    ) * marginal_land_yield_multiplier_from_capital() / land_yield_multiplier_from_capital()


@cache('step')
def marginal_productivity_of_land_development():
    """
    marginal productivity of land development

    Veg eq kg/$

    component

    The marginal productivity of land development (MPLD#109)
    """
    return land_yield() / (development_cost_per_hectare() * social_discount())


@cache('run')
def social_discount():
    """
    social discount

    1/year

    constant

    SOCIAL DISCOUNT (SD#109.1)
    """
    return 0.07


